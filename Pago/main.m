//
//  main.m
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
