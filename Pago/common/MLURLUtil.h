//
//  MLURLUtil.h
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MLURLUtil : NSObject

/**
 * Arma una URL a partir de una URL base y una URI.
 *
 * @param baseUrl URL base a utilizar
 * @param uri Path a gregar
 * @return URL armada
 */
+ (NSString *)urlWithBaseURL:(NSString *)baseUrl andURI:(NSString *)uri;

@end
