//
//  MLConfig.h
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MLConfig : NSObject

/**
 * Devuelve la instancia única de la clase
 *
 * @return instancia única de la clase
 */
+ (MLConfig *)shared;

/**
 * Obtiene los valores configurados para la aplicación.
 *
 * @param key Clave de la configuración
 * @return Valor asociado a la clave
 */
- (NSString *)configuration:(NSString *)key;

@end
