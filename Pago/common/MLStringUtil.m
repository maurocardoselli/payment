//
//  MLStringUtil.m
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "MLStringUtil.h"

@implementation MLStringUtil

+ (BOOL)isEmpty:(NSString *)text {
    return (!text || [text isKindOfClass:[NSNull class]] || text.length == 0);
}

+ (NSString *)null2String:(NSString *)text {
    return (!text ? @"" : text);
}

+ (NSString *)formatNumber:(NSNumber *)number withFractionDigits:(NSUInteger)decimals {
    return [self formatNumber:number withMinFractionDigits:decimals andMaxFractionDigits:decimals];
}

+ (NSString *)formatNumber:(NSNumber *)number withMinFractionDigits:(NSUInteger)min andMaxFractionDigits:(NSUInteger)max {
    number = [self null2Number:number];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.roundingMode = NSNumberFormatterRoundCeiling;
    formatter.minimumFractionDigits = min;
    formatter.maximumFractionDigits = max;
    formatter.usesGroupingSeparator = NO;
    formatter.decimalSeparator = @".";
    return [formatter stringFromNumber:number];
}

+ (NSNumber *)null2Number:(NSNumber *)number {
    if (!number || number == NULL || [number isEqual:[NSDecimalNumber notANumber]])
        number=[[NSDecimalNumber alloc] initWithInt:0];
    return number;
}

@end
