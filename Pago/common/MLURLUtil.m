//
//  MLURLUtil.m
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "MLURLUtil.h"
#import "MLConfig.h"

@implementation MLURLUtil

+ (NSString *)urlWithBaseURL:(NSString *)baseUrl andURI:(NSString *)uri {
    return [baseUrl stringByAppendingString:uri];
}

@end
