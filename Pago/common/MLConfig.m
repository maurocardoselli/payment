//
//  MLConfig.m
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "MLConfig.h"

#define CONFIG_FILE @"Info"

static MLConfig *__shared;

@implementation MLConfig {
    NSDictionary *_values;
}

+ (MLConfig *)shared {
    if (!__shared)
        __shared = [MLConfig new];
    return __shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        // Obtengo el archivo con la configuración y cargo el diccionario
        NSString *filePath = [[NSBundle mainBundle] pathForResource:CONFIG_FILE ofType:@"plist"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            _values = [NSDictionary dictionaryWithContentsOfFile:filePath];
        } else {
            _values = [NSDictionary new];
        }
    }
    return self;
}

- (NSString *)configuration:(NSString *)key {
    return [_values objectForKey:key];
}

@end
