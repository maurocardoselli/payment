//
//  MLStringUtil.h
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MLStringUtil : NSError

/**
 * Indica si una cadena está vacía.
 *
 * @param text Cadena a verificar
 * @return YES si la cadena está vacia o es NULL, NO en cualquier otro caso
 */
+ (BOOL)isEmpty:(NSString *)text;

/**
 * Verifica si una cadena es NULL y la transforma a una cadena vacía.
 *
 * @param text Cadena a verificar
 * @return Cadena vacía en caso de ser null, sino la cadena tal cual está
 */
+ (NSString *)null2String:(NSString *)text;

/**
 * Formatea un número con la cantidad de decimales indicada. El separador decimal es ".".
 *
 * @param number Número a formatear
 * @param decimals Cantidad de decimales del número formateado
 * @return el número con el formato especificado.
 */
+ (NSString *)formatNumber:(NSNumber *)number withFractionDigits:(NSUInteger)decimals;

@end
