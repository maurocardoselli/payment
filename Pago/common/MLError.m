//
//  MLError.m
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "MLError.h"

#define ERROR_DOMAIN @"com.ml.pago"

@implementation MLError

+ (instancetype)errorWithError:(NSError *)error {
    if (error)
        return [MLError errorWithDomain:error.domain code:error.code userInfo:error.userInfo];
    return nil;
}

+ (instancetype)errorWithMessage:(NSString *)message {
    NSString *messageStr = (!message ? @"" : message);
    NSMutableDictionary *userInfo = [NSMutableDictionary new];
    [userInfo setObject:messageStr forKey:NSLocalizedDescriptionKey];
    return [MLError errorWithDomain:ERROR_DOMAIN code:666 userInfo:userInfo];
}

+ (instancetype)errorWithError:(NSError *)error andMessage:(NSString *)message {
    if (error) {
        NSString *messageStr = (!message ? @"" : message);
        if ((!messageStr || [messageStr isKindOfClass:[NSNull class]] || messageStr.length == 0))
            messageStr = [error.userInfo valueForKey:NSLocalizedDescriptionKey];
        NSMutableDictionary *userInfo = [NSMutableDictionary new];
        [userInfo setObject:messageStr forKey:NSLocalizedDescriptionKey];
        return [MLError errorWithDomain:error.domain code:error.code userInfo:userInfo];
    }
    return nil;
}

@end
