//
//  MLError.h
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MLError : NSError

+ (instancetype)errorWithError:(NSError *)error;
+ (instancetype)errorWithMessage:(NSString *)message;
+ (instancetype)errorWithError:(NSError *)error andMessage:(NSString *)message;

@end
