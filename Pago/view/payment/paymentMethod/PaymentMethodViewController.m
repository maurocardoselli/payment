//
//  PaymentMethodViewController.m
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentMethodViewController.h"
#import "PaymentMethodService.h"
#import "PaymentMethodCell.h"
#import <SVProgressHUD/SVProgressHUD.h>


@interface PaymentMethodViewController () <UITableViewDelegate, UITableViewDataSource> {
    NSArray *_data;
}

@end

@implementation PaymentMethodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addDelegates];
    [self removeExtraRows];
    [self refreshPaymentMethods];
}

- (void)addDelegates {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

/**
 * Elimino el footer de la tabla para que no se vean registros adicionales.
 */
- (void)removeExtraRows {
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

/**
 * Carga los datos en la tabla.
 */
- (void)loadData:(NSArray *)data {
    // Cargo los datos en la pantalla y recargo la tabla
    _data = data;
    [self.tableView reloadData];
}

/**
 * Obtengo los datos desde la API y los cargo en la tabla.
 */
- (void)refreshPaymentMethods {
    [SVProgressHUD showWithStatus:@"Cargando..."];
    
    __weak PaymentMethodViewController *weakSelf = self;
    // Obtengo los datos
    [[PaymentMethodService new] getByType:PaymentType_CREDIT_CARD onSuccess:^(NSArray *paymentMethods) {
        __strong PaymentMethodViewController *strongSelf = weakSelf;
        [strongSelf loadData:paymentMethods];
        
        [SVProgressHUD dismiss];
    } onFailure:^(MLError *error) {
        [SVProgressHUD showErrorWithStatus:@"Surgió un error"];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _data.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PaymentMethodCell *cell = [tableView dequeueReusableCellWithIdentifier:@"paymentMethodCell"];
    if (!cell) {
        // Si no está registrada, la registro y vuelvo a cargarla
        [tableView registerNib:[UINib nibWithNibName:@"PaymentMethodCell" bundle:nil] forCellReuseIdentifier:@"paymentMethodCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"paymentMethodCell"];
    }
    
    // Cargo el medio de pago en el cell
    PaymentMethod *paymentMethod = [_data objectAtIndex:indexPath.row];
    cell.paymentMethod = paymentMethod;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Obtengo el medio de pago seleccionado y lo asocio al pago
    PaymentMethod *paymentMethod = [self selectedPaymentMethod];
    self.payment.paymentMethod = paymentMethod;
    // Avanzo a la siguiente pantalla
    [self performSegueWithIdentifier:@"paymentScreen" sender:nil];
}

/**
 * Obtengo el medio de pago seleccionado.
 *
 * @return El medio de pago según la seleccion en la tabla
 */
- (PaymentMethod *)selectedPaymentMethod {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    PaymentMethod *paymentMethod = [_data objectAtIndex:indexPath.row];
    return paymentMethod;
}

@end
