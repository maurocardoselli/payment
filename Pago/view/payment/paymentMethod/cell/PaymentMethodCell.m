//
//  PaymentMethodCell.m
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentMethodCell.h"
#import <SDWebImage/UIView+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PaymentMethodCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setPaymentMethod:(PaymentMethod *)paymentMethod {
    _paymentMethod = paymentMethod;
    
    self.nameLabel.text = paymentMethod.name;
    [self.thumbnailImage sd_setShowActivityIndicatorView:YES];
    [self.thumbnailImage sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.thumbnailImage sd_setImageWithURL:[NSURL URLWithString:paymentMethod.secureThumbnail]];
}

@end
