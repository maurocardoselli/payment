//
//  PaymentMethodCell.h
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentMethod.h"

@interface PaymentMethodCell : UITableViewCell

@property (nonatomic) PaymentMethod *paymentMethod;

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
