//
//  InstallmentCell.m
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "InstallmentCell.h"

@implementation InstallmentCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setInstallment:(Installment *)installment {
    _installment = installment;
    self.recommendedMessageLabel.text = installment.recommendedMessage;
}

@end
