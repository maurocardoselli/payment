//
//  InstallmentCell.h
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Installment.h"

@interface InstallmentCell : UITableViewCell

@property (nonatomic) Installment *installment;

@property (weak, nonatomic) IBOutlet UILabel *recommendedMessageLabel;

@end
