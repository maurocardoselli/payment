//
//  InstallmentViewController.m
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "InstallmentViewController.h"
#import "InstallmentService.h"
#import "InstallmentCell.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface InstallmentViewController () <UITableViewDelegate, UITableViewDataSource> {
    NSArray *_data;
}

@end

@implementation InstallmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addDelegates];
    [self removeExtraRows];
    [self refreshCardIssuers];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)addDelegates {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

/**
 * Elimino el footer de la tabla para que no se vean registros adicionales.
 */
- (void)removeExtraRows {
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

/**
 * Carga los datos en la tabla.
 */
- (void)loadData:(NSArray *)data {
    // Cargo los datos en la pantalla y recargo la tabla
    _data = data;
    [self.tableView reloadData];
}

/**
 * Obtengo los datos desde la API y los cargo en la tabla.
 */
- (void)refreshCardIssuers {
    [SVProgressHUD showWithStatus:@"Cargando..."];
    
    __weak InstallmentViewController *weakSelf = self;
    // Obtengo los datos
    [[InstallmentService new] getByCardIssuerId:self.payment.cardIssuer.id
                            withPaymentMethodId:self.payment.paymentMethod.id
                                      andAmount:self.payment.amount
                                      onSuccess:^(NSArray *installments) {
          
          __strong InstallmentViewController *strongSelf = weakSelf;
          [strongSelf loadData:installments];
                                          
          [SVProgressHUD dismiss];
    } onFailure:^(MLError *error) {
        [SVProgressHUD showErrorWithStatus:@"Surgió un error"];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _data.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InstallmentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"installmentCell"];
    if (!cell) {
        // Si no está registrada, la registro y vuelvo a cargarla
        [tableView registerNib:[UINib nibWithNibName:@"InstallmentCell" bundle:nil] forCellReuseIdentifier:@"installmentCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"installmentCell"];
    }
    
    // Cargo las cuotas en el cell
    Installment *installment = [_data objectAtIndex:indexPath.row];
    cell.installment = installment;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Obtengo las cuotas seleccionadas y las asocio al pago
    Installment *installment = [self selectedInstallment];
    self.payment.installment = installment;
    
    // Pregunto si quiere confirmar
    [self showConfirmAlert];
}

/**
 * Obtengo las cuotas seleccionadas
 *
 * @return Las cuotas según la seleccion en la tabla
 */
- (Installment *)selectedInstallment {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    Installment *installment = [_data objectAtIndex:indexPath.row];
    return installment;
}

/**
 * Muestra un alert consultando si quiere o no confirmar el pago. Si presiona que SI se avanza,
 * sino sólo desaparece el alert.
 */
- (void)showConfirmAlert {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Confirmar" message:@"¿Desea confirmar el pago?" preferredStyle:UIAlertControllerStyleAlert];
    
    __weak InstallmentViewController *weakSelf = self;
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Si" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        // Si confirma
        __strong InstallmentViewController *strongSelf = weakSelf;
        [strongSelf confirmPayment];
    }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

/**
 * Finaliza el proceso y vuelve a la pantalla principal.
 */
- (void)confirmPayment {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.paymentDelegate paymentConfirmed:self.payment];
}

@end
