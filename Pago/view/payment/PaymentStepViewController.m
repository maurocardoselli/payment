//
//  PaymentStepViewController.m
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentStepViewController.h"

@interface PaymentStepViewController ()

@end

@implementation PaymentStepViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"paymentScreen"]) {
        PaymentStepViewController *vc = segue.destinationViewController;
        vc.payment = self.payment;
        vc.paymentDelegate = self.paymentDelegate;
    }
}

@end
