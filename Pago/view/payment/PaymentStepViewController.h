//
//  PaymentStepViewController.h
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Payment.h"

@protocol PaymentDelegate <NSObject>
@required
/**
 * Confirma un pago indicando cual es.
 *
 * @param payment Pago confirmado.
 */
- (void)paymentConfirmed:(Payment *)payment;

@end

@interface PaymentStepViewController : UIViewController

@property (nonatomic) Payment *payment;
@property (nonatomic) id<PaymentDelegate> paymentDelegate;

@end
