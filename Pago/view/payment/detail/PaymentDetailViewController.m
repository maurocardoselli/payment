//
//  PaymentDetailViewController.m
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentDetailViewController.h"
#import "MLStringUtil.h"

@interface PaymentDetailViewController ()

@end

@implementation PaymentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Cargo los datos en pantalla
    self.amountLabel.text = [MLStringUtil formatNumber:self.payment.amount withFractionDigits:2];
    self.paymentMethodLabel.text = self.payment.paymentMethod.name;
    self.cardIssuerLabel.text = self.payment.cardIssuer.name;
    self.installmentLabel.text = self.payment.installment.recommendedMessage;
}

- (IBAction)onDone:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
