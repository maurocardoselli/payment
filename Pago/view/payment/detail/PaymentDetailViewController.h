//
//  PaymentDetailViewController.h
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Payment.h"

@interface PaymentDetailViewController : UIViewController

@property (nonatomic) Payment *payment;

@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentMethodLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardIssuerLabel;
@property (weak, nonatomic) IBOutlet UILabel *installmentLabel;

- (IBAction)onDone:(id)sender;

@end
