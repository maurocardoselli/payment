//
//  PaymentAmountViewController.m
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentAmountViewController.h"
#import "MLStringUtil.h"

@interface PaymentAmountViewController () <UITextFieldDelegate>

@end

@implementation PaymentAmountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createControls];
    [self addDelegates];
}

- (void)createControls {
    // Cargo el valor inicial
    self.amountField.text = @"0.00";
    // Pongo el foco en este campo
    [self.amountField becomeFirstResponder];
}

- (void)addDelegates {
    [self.amountField addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"paymentScreen"]) {
        // FIxME: agregar validacion
        self.payment.amount = [NSNumber numberWithDouble:[self.amountField.text doubleValue]];
        return [self isValidAmount];
    }
    return YES;
}

/**
 * Verifica si el monto ingresado es válido.
 *
 * @return YES si el monto es válido, caso contrario devuelve NO
 */
- (BOOL)isValidAmount {
    return ([self.payment.amount doubleValue] > 0);
}


#pragma mark - Payment Delegate

/**
 * Se encarga de formatear el texto ingresado para que solo admita números y muestre dos decimales.
 *
 * @param textField Campo de texto a formatear
 */
- (void)textFieldDidChange:(UITextField *)textField {
    
    NSString *text = textField.text;
    
    NSError *error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^0-9]" options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSString *cleanAmount = [regex stringByReplacingMatchesInString:text
                                                            options:0
                                                              range:NSMakeRange(0, [text length])
                                                       withTemplate:@""];
    
    
    double value = [cleanAmount doubleValue];
    NSNumber *amount = [NSNumber numberWithDouble:(value/100)];
    NSString *formattedAmount = [MLStringUtil formatNumber:amount withFractionDigits:2];
    textField.text = formattedAmount;
}

@end
