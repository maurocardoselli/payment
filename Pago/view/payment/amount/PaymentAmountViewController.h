//
//  PaymentAmountViewController.h
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentStepViewController.h"

@interface PaymentAmountViewController : PaymentStepViewController

@property (weak, nonatomic) IBOutlet UITextField *amountField;

@end
