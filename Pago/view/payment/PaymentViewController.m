//
//  PaymentViewController.m
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentViewController.h"
#import "Payment.h"
#import "PaymentStepViewController.h"
#import "PaymentDetailViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface PaymentViewController () <PaymentDelegate> {
    Payment *_payment;
}

@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createControls];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)createControls {
    // Seteo el modo del indicador de progreso
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"paymentScreen"]) {
        PaymentStepViewController *vc = segue.destinationViewController;
        vc.payment = [self createPayment];
        vc.paymentDelegate = self;
    } else if ([segue.identifier isEqualToString:@"showPayment"]) {
        PaymentDetailViewController *vc = segue.destinationViewController;
        vc.payment = _payment;
    }
}

/**
 * Devuelve un nuevo pago a configurar.
 */
- (Payment *)createPayment {
    return [Payment new];
}


#pragma mark - Payment Delegate

/**
 * Al confirmar el pago, muestra una pantalla con el detalle del mismo.
 */
- (void)paymentConfirmed:(Payment *)payment {
    _payment = payment;
    [self performSegueWithIdentifier:@"showPayment" sender:nil];
}

@end
