//
//  CardIssuerViewController.m
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "CardIssuerViewController.h"
#import "CardIssuerService.h"
#import "CardIssuerCell.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface CardIssuerViewController () <UITableViewDelegate, UITableViewDataSource> {
    NSArray *_data;
}

@end

@implementation CardIssuerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addDelegates];
    [self removeExtraRows];
    [self refreshCardIssuers];
}

- (void)addDelegates {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

/**
 * Elimino el footer de la tabla para que no se vean registros adicionales.
 */
- (void)removeExtraRows {
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

/**
 * Carga los datos en la tabla.
 */
- (void)loadData:(NSArray *)data {
    // Cargo los datos en la pantalla y recargo la tabla
    _data = data;
    [self.tableView reloadData];
}

/**
 * Obtengo los datos desde la API y los cargo en la tabla.
 */
- (void)refreshCardIssuers {
    [SVProgressHUD showWithStatus:@"Cargando..."];
    
    __weak CardIssuerViewController *weakSelf = self;
    // Obtengo los datos
    [[CardIssuerService new] getByPaymentMethodId:self.payment.paymentMethod.id onSuccess:^(NSArray *cardIssuers) {
        __strong CardIssuerViewController *strongSelf = weakSelf;
        [strongSelf loadData:cardIssuers];
        
        [SVProgressHUD dismiss];
    } onFailure:^(MLError *error) {
        [SVProgressHUD showErrorWithStatus:@"Surgió un error"];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _data.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CardIssuerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cardIssuerCell"];
    if (!cell) {
        // Si no está registrada, la registro y vuelvo a cargarla
        [tableView registerNib:[UINib nibWithNibName:@"CardIssuerCell" bundle:nil] forCellReuseIdentifier:@"cardIssuerCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"cardIssuerCell"];
    }
    
    // Cargo el banco en el cell
    CardIssuer *cardIssuer = [_data objectAtIndex:indexPath.row];
    cell.cardIssuer = cardIssuer;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Obtengo el banco seleccionado y lo asocio al pago
    CardIssuer *cardIssuer = [self selectedCardIssuer];
    self.payment.cardIssuer = cardIssuer;
    // Avanzo a la siguiente pantalla
    [self performSegueWithIdentifier:@"paymentScreen" sender:nil];
}

/**
 * Obtengo el banco seleccionado.
 *
 * @return El banco según la seleccion en la tabla
 */
- (CardIssuer *)selectedCardIssuer {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    CardIssuer *cardIssuer = [_data objectAtIndex:indexPath.row];
    return cardIssuer;
}

@end
