//
//  CardIssuer.m
//  Pago
//
//  Created by Mauro Cardoselli on 26/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "CardIssuer.h"
#import "Constants.h"

@implementation CardIssuer

+ (CardIssuer *)JSON2CardIssuer:(NSDictionary *)json {
    CardIssuer *cardIssuer = [self new];
    cardIssuer.id = [json valueForKey:JSON_CARD_ISSUER_ID];
    cardIssuer.name = [json valueForKey:JSON_CARD_ISSUER_NAME];
    cardIssuer.thumbnail = [json valueForKey:JSON_CARD_ISSUER_THUMBNAIL];
    cardIssuer.secureThumbnail = [json valueForKey:JSON_CARD_ISSUER_SECURE_THUMBNAIL];
    cardIssuer.processingMode = [json valueForKey:JSON_CARD_ISSUER_PROCESSING_MODE];
    cardIssuer.merchantAccountId = [json valueForKey:JSON_CARD_ISSUER_MERCHANT_ACCOUNT_ID];
    return cardIssuer;
}

+ (NSArray *)JSON2CardIssuers:(NSArray *)json {
    NSMutableArray *cardIssuers = [NSMutableArray new];
    for (NSDictionary *j in json) {
        CardIssuer *cardIssuer = [self JSON2CardIssuer:j];
        if (cardIssuer)
            [cardIssuers addObject:cardIssuer];
    }
    return cardIssuers;
}

@end
