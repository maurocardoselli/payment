//
//  Installment.m
//  Pago
//
//  Created by Mauro Cardoselli on 26/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "Installment.h"
#import "Constants.h"

@implementation Installment

+ (Installment *)JSON2Installment:(NSDictionary *)json {
    Installment *cardIssuer = [self new];
    cardIssuer.installments = [json valueForKey:JSON_INSTALLMENT_INSTALLMENTS];
    cardIssuer.installmentRate = [json valueForKey:JSON_INSTALLMENT_INSTALLMENT_RATE];
    cardIssuer.discountRate = [json valueForKey:JSON_INSTALLMENT_DISCOUNT_RATE];
    cardIssuer.labels = [json valueForKey:JSON_INSTALLMENT_LABELS];
    cardIssuer.installmentRateCollector = [json valueForKey:JSON_INSTALLMENT_INSTALLMENT_RATE_COLLECTOR];
    cardIssuer.recommendedMessage = [json valueForKey:JSON_INSTALLMENT_RECOMMENDED_MESSAGE];
    cardIssuer.installmentAmount = [json valueForKey:JSON_INSTALLMENT_INSTALLMENT_AMOUNT];
    cardIssuer.totalAmount = [json valueForKey:JSON_INSTALLMENT_TOTAL_AMOUNT];
    return cardIssuer;
}

+ (NSArray *)JSON2Installments:(NSArray *)json {
    NSMutableArray *installments = [NSMutableArray new];
    for (NSDictionary *j in json) {
        NSArray *payerCosts = [j valueForKey:JSON_INSTALLMENT_PAYER_COSTS];
        for (NSDictionary *payerCost in payerCosts) {
            Installment *installment = [self JSON2Installment:payerCost];
            if (installment)
                [installments addObject:installment];
        }
    }
    return installments;
}

@end
