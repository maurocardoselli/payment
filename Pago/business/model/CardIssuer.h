//
//  CardIssuer.h
//  Pago
//
//  Created by Mauro Cardoselli on 26/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardIssuer : NSObject

@property (nonatomic) NSString *id;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *thumbnail;
@property (nonatomic) NSString *secureThumbnail;
@property (nonatomic) NSString *processingMode;
@property (nonatomic) NSString *merchantAccountId;

+ (CardIssuer *)JSON2CardIssuer:(NSDictionary *)json;
+ (NSArray *)JSON2CardIssuers:(NSArray *)json;

@end
