//
//  PaymentMethod.m
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentMethod.h"
#import "Constants.h"

@implementation PaymentMethod

+ (PaymentMethod *)JSON2PaymentMethod:(NSDictionary *)json {
    PaymentMethod *paymentMethod = [self new];
    paymentMethod.id = [json valueForKey:JSON_PAYMENT_METHOD_ID];
    paymentMethod.name = [json valueForKey:JSON_PAYMENT_METHOD_NAME];
    paymentMethod.status = [json valueForKey:JSON_PAYMENT_METHOD_STATUS];
    paymentMethod.thumbnail = [json valueForKey:JSON_PAYMENT_METHOD_THUMBNAIL];
    paymentMethod.secureThumbnail = [json valueForKey:JSON_PAYMENT_METHOD_SECURE_THUMBNAIL];
    paymentMethod.minAllowedAmount = [json valueForKey:JSON_PAYMENT_METHOD_MIN_ALLOWED_AMOUNT];
    paymentMethod.maxAllowedAmount = [json valueForKey:JSON_PAYMENT_METHOD_MAX_ALLOWED_AMOUNT];
    
    paymentMethod.paymentType = [self findPaymentMethodType:[json valueForKey:JSON_PAYMENT_METHOD_TYPE_ID]];
    
    return paymentMethod;
}

+ (NSArray *)JSON2PaymentMethods:(NSArray *)json {
    NSMutableArray *paymentMethods = [NSMutableArray new];
    for (NSDictionary *j in json) {
        PaymentMethod *paymentMethod = [self JSON2PaymentMethod:j];
        if (paymentMethod)
            [paymentMethods addObject:paymentMethod];
    }
    return paymentMethods;
}

+ (PaymentType)findPaymentMethodType:(NSString *)state {
    if ([state isEqualToString:@"ticket"])
        return PaymentType_TICKET;
    if ([state isEqualToString:@"atm"])
        return PaymentType_ATM;
    if ([state isEqualToString:@"credit_card"])
        return PaymentType_CREDIT_CARD;
    if ([state isEqualToString:@"debit_card"])
        return PaymentType_DEBIT_CARD;
    if ([state isEqualToString:@"prepaid_card"])
        return PaymentType_PREPAID_CARD;
    
    // Valor por defecto: credit_card
    return PaymentType_CREDIT_CARD;
}

+ (NSString *)paymentMethodType2String:(PaymentType)type {
    switch (type) {
        case PaymentType_TICKET:
            return @"ticket";
        case PaymentType_ATM:
            return @"atm";
        case PaymentType_CREDIT_CARD:
            return @"credit_card";
        case PaymentType_DEBIT_CARD:
            return @"debit_card";
        case PaymentType_PREPAID_CARD:
            return @"prepaid_card";
    }
    return nil;
}

@end
