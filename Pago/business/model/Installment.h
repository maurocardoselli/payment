//
//  Installment.h
//  Pago
//
//  Created by Mauro Cardoselli on 26/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Installment : NSObject

@property (nonatomic) NSNumber *installments;
@property (nonatomic) NSNumber *installmentRate;
@property (nonatomic) NSNumber *discountRate;
@property (nonatomic) NSArray *labels;
@property (nonatomic) NSArray *installmentRateCollector;
@property (nonatomic) NSString *recommendedMessage;
@property (nonatomic) NSNumber *installmentAmount;
@property (nonatomic) NSNumber *totalAmount;

+ (Installment *)JSON2Installment:(NSDictionary *)json;
+ (NSArray *)JSON2Installments:(NSArray *)json;

@end
