//
//  Payment.h
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentMethod.h"
#import "CardIssuer.h"
#import "Installment.h"

@interface Payment : NSObject

@property (nonatomic) NSNumber *amount;
@property (nonatomic) PaymentMethod *paymentMethod;
@property (nonatomic) CardIssuer *cardIssuer;
@property (nonatomic) Installment *installment;

@end
