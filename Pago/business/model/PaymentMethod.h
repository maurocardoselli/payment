//
//  PaymentMethod.h
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    PaymentType_TICKET,
    PaymentType_ATM,
    PaymentType_CREDIT_CARD,
    PaymentType_DEBIT_CARD,
    PaymentType_PREPAID_CARD,
} PaymentType;

@interface PaymentMethod : NSObject

@property (nonatomic) NSString *id;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *paymentTypeId;
@property (nonatomic) PaymentType paymentType;
@property (nonatomic) NSString *status;
@property (nonatomic) NSString *thumbnail;
@property (nonatomic) NSString *secureThumbnail;
@property (nonatomic) NSNumber *minAllowedAmount;
@property (nonatomic) NSNumber *maxAllowedAmount;

+ (PaymentMethod *)JSON2PaymentMethod:(NSDictionary *)json;
+ (NSArray *)JSON2PaymentMethods:(NSArray *)json;

+ (NSString *)paymentMethodType2String:(PaymentType)type;

@end
