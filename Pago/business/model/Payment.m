//
//  Payment.m
//  Pago
//
//  Created by Mauro Cardoselli on 30/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "Payment.h"

@implementation Payment

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.amount = 0;
    }
    return self;
}

@end
