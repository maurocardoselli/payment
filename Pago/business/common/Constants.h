//
//  Constants.h
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

//////////////////////////////////////
// SERVER INFO
//////////////////////////////////////

// PATHS
#define PAYMENT_METHOD_URI @"/v1/payment_methods"
#define CARD_ISSUER_URI @"/v1/payment_methods/card_issuers"
#define INSTALLMENT_URI @"/v1/payment_methods/installments"
// QUERY PARAMS
#define SERVER_PUBLIC_KEY_PARAM @"public_key"
#define PAYMENT_METHOD_PAYMENT_TYPE_PARAM @"payment_type_id"
#define CARD_ISSUER_PAYMENT_METHOD_PARAM @"payment_method_id"
#define CARD_ISSUER_ISSUER_PARAM @"issuer.id"
#define CARD_ISSUER_AMOUNT_PARAM @"amount"

// PAYMENT METHOD
#define JSON_PAYMENT_METHOD_ID @"id"
#define JSON_PAYMENT_METHOD_NAME @"name"
#define JSON_PAYMENT_METHOD_TYPE_ID @"payment_type_id"
#define JSON_PAYMENT_METHOD_STATUS @"status"
#define JSON_PAYMENT_METHOD_THUMBNAIL @"thumbnail"
#define JSON_PAYMENT_METHOD_SECURE_THUMBNAIL @"secure_thumbnail"
#define JSON_PAYMENT_METHOD_MIN_ALLOWED_AMOUNT @"min_allowed_amount"
#define JSON_PAYMENT_METHOD_MAX_ALLOWED_AMOUNT @"max_allowed_amount"

// CARD ISSUER
#define JSON_CARD_ISSUER_ID @"id"
#define JSON_CARD_ISSUER_NAME @"name"
#define JSON_CARD_ISSUER_THUMBNAIL @"thumbnail"
#define JSON_CARD_ISSUER_SECURE_THUMBNAIL @"secure_thumbnail"
#define JSON_CARD_ISSUER_PROCESSING_MODE @"processing_mode"
#define JSON_CARD_ISSUER_MERCHANT_ACCOUNT_ID @"merchant_account_id"

// INSTALLMENT
#define JSON_INSTALLMENT_PAYER_COSTS @"payer_costs"
#define JSON_INSTALLMENT_INSTALLMENTS @"installments"
#define JSON_INSTALLMENT_INSTALLMENT_RATE @"installment_rate"
#define JSON_INSTALLMENT_DISCOUNT_RATE @"discount_rate"
#define JSON_INSTALLMENT_LABELS @"labels"
#define JSON_INSTALLMENT_INSTALLMENT_RATE_COLLECTOR @"installment_rate_collector"
#define JSON_INSTALLMENT_RECOMMENDED_MESSAGE @"recommended_message"
#define JSON_INSTALLMENT_INSTALLMENT_AMOUNT @"installment_amount"
#define JSON_INSTALLMENT_TOTAL_AMOUNT @"total_amount"

