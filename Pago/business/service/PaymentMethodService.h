//
//  PaymentMethodService.h
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentMethod.h"
#import "MLError.h"

@interface PaymentMethodService : NSObject

- (void)getByType:(PaymentType)type
        onSuccess:(void (^)(NSArray *paymentMethods))success
        onFailure:(void (^)(MLError *error))failure;

@end
