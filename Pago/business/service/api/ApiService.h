//
//  ApiService.h
//  Pago
//
//  Created by Mauro Cardoselli on 27/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLError.h"

@interface ApiService : NSObject

- (void)getFromBaseURL:(NSString *)baseUrl
               withURI:(NSString *)uri
         andParameters:(NSDictionary *)parameters
             onSuccess:(void (^)(NSArray *result))success
             onFailure:(void (^)(MLError *error))failure;

- (void)getFromBaseURL:(NSString *)baseUrl
               withURI:(NSString *)uri
             onSuccess:(void (^)(NSArray *result))success
             onFailure:(void (^)(MLError *error))failure;

@end
