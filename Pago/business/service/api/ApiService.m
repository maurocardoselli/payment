//
//  ApiService.m
//  Pago
//
//  Created by Mauro Cardoselli on 27/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "ApiService.h"
#import "MLURLUtil.h"
#import <AFNetworking/AFNetworking.h>

@implementation ApiService

- (void)getFromBaseURL:(NSString *)baseUrl
               withURI:(NSString *)uri
         andParameters:(NSDictionary *)parameters
             onSuccess:(void (^)(NSArray *))success
             onFailure:(void (^)(MLError *))failure {
    
    // Valido datos necesarios para el armado de la url
    if (!baseUrl) {
        failure([MLError errorWithMessage:@"Se requiere una url base"]);
        return;
    }
    if (!uri) {
        failure([MLError errorWithMessage:@"Se requiere una uri"]);
        return;
    }
    
    // Armo la URL
    NSString *url = [MLURLUtil urlWithBaseURL:baseUrl andURI:uri];
    
    // Armo el request
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        // En este caso no necesito informar el progreso
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        // Retorno el array obtenido
        success([NSArray arrayWithArray:responseObject]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure([MLError errorWithError:error]);
    }];
}

- (void)getFromBaseURL:(NSString *)baseUrl
               withURI:(NSString *)uri
             onSuccess:(void (^)(NSArray *))success
             onFailure:(void (^)(MLError *))failure {
    
    [self getFromBaseURL:baseUrl
                 withURI:uri
           andParameters:nil
               onSuccess:success
               onFailure:failure];
}

@end
