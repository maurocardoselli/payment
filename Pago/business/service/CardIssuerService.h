//
//  CardIssuerService.h
//  Pago
//
//  Created by Mauro Cardoselli on 26/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLError.h"

@interface CardIssuerService : NSObject

- (void)getByPaymentMethodId:(NSString *)paymentMethodId
                   onSuccess:(void (^)(NSArray *cardIssuers))success
                   onFailure:(void (^)(MLError *error))failure;

@end
