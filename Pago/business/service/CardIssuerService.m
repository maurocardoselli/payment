//
//  CardIssuerService.m
//  Pago
//
//  Created by Mauro Cardoselli on 26/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "CardIssuerService.h"
#import "Constants.h"
#import "MLURLUtil.h"
#import <AFNetworking/AFNetworking.h>
#import "MLConfig.h"
#import "CardIssuer.h"
#import "ApiService.h"

#define SERVER_BASE_URL @"SERVER_BASE_URL"
#define SERVER_PUBLIC_KEY @"SERVER_PUBLIC_KEY"

@implementation CardIssuerService

- (void)getByPaymentMethodId:(NSString *)paymentMethodId
                   onSuccess:(void (^)(NSArray *))success
                   onFailure:(void (^)(MLError *))failure {
    
    // Datos para url
    NSString *baseUrl = [[MLConfig shared] configuration:SERVER_BASE_URL];
    NSString *uri = CARD_ISSUER_URI;
    
    // Parametros
    NSString *publicKey = [[MLConfig shared] configuration:SERVER_PUBLIC_KEY];
    NSDictionary *parameters = [self queryParametersWithPublicKey:publicKey
                                               andPaymentMethodId:paymentMethodId];
    
    // Ejecuto el servicio
    [[ApiService new] getFromBaseURL:baseUrl
                             withURI:uri
                       andParameters:parameters
                           onSuccess:^(NSArray *result) {
                               
       // Devuelvo el array de models
       success([CardIssuer JSON2CardIssuers:result]);
   } onFailure:^(MLError *error) {
       failure(error);
   }];
}

- (NSDictionary *)queryParametersWithPublicKey:(NSString *)publicKey
                            andPaymentMethodId:(NSString *)paymentMethodId {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    [parameters setValue:publicKey forKey:SERVER_PUBLIC_KEY_PARAM];
    [parameters setValue:paymentMethodId forKey:CARD_ISSUER_PAYMENT_METHOD_PARAM];
    return parameters;
}

@end
