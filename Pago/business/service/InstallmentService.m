//
//  InstallmentService.m
//  Pago
//
//  Created by Mauro Cardoselli on 26/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "InstallmentService.h"
#import "Constants.h"
#import "MLURLUtil.h"
#import <AFNetworking/AFNetworking.h>
#import "MLConfig.h"
#import "Installment.h"
#import "ApiService.h"

#define SERVER_BASE_URL @"SERVER_BASE_URL"
#define SERVER_PUBLIC_KEY @"SERVER_PUBLIC_KEY"

@implementation InstallmentService

- (void)getByCardIssuerId:(NSString *)cardIssuerId
      withPaymentMethodId:(NSString *)paymentMethodId
                andAmount:(NSNumber *)amount
                onSuccess:(void (^)(NSArray *installments))success
                onFailure:(void (^)(MLError *error))failure {
    
    // Datos para url
    NSString *baseUrl = [[MLConfig shared] configuration:SERVER_BASE_URL];
    NSString *uri = INSTALLMENT_URI;
    
    // Parametros
    NSString *publicKey = [[MLConfig shared] configuration:SERVER_PUBLIC_KEY];
    NSDictionary *parameters = [self queryParametersWithPublicKey:publicKey
                                                  andCardIssuerId:cardIssuerId
                                               andPaymentMethodId:paymentMethodId
                                                        andAmount:amount];
    // Ejecuto el servicio
    [[ApiService new] getFromBaseURL:baseUrl
                             withURI:uri
                       andParameters:parameters
                           onSuccess:^(NSArray *result) {
                               
       // Devuelvo el array de models
       success([Installment JSON2Installments:result]);
    } onFailure:^(MLError *error) {
       failure(error);
    }];
}

- (NSDictionary *)queryParametersWithPublicKey:(NSString *)publicKey
                               andCardIssuerId:(NSString *)cardIssuerId
                            andPaymentMethodId:(NSString *)paymentMethodId
                                     andAmount:(NSNumber *)amount{
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    [parameters setValue:publicKey forKey:SERVER_PUBLIC_KEY_PARAM];
    [parameters setValue:paymentMethodId forKey:CARD_ISSUER_PAYMENT_METHOD_PARAM];
    [parameters setValue:cardIssuerId forKey:CARD_ISSUER_ISSUER_PARAM];
    [parameters setValue:amount forKey:CARD_ISSUER_AMOUNT_PARAM];
    return parameters;
}

@end
