//
//  InstallmentService.h
//  Pago
//
//  Created by Mauro Cardoselli on 26/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLError.h"

@interface InstallmentService : NSObject

- (void)getByCardIssuerId:(NSString *)cardIssuerId
      withPaymentMethodId:(NSString *)paymentMethodId
                andAmount:(NSNumber *)amount
                onSuccess:(void (^)(NSArray *installments))success
                onFailure:(void (^)(MLError *error))failure;

@end
