//
//  PaymentMethodService.m
//  Pago
//
//  Created by Mauro Cardoselli on 25/11/2018.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentMethodService.h"
#import "Constants.h"
#import "MLURLUtil.h"
#import <AFNetworking/AFNetworking.h>
#import "MLConfig.h"
#import "ApiService.h"

#define SERVER_BASE_URL @"SERVER_BASE_URL"
#define SERVER_PUBLIC_KEY @"SERVER_PUBLIC_KEY"

@implementation PaymentMethodService

- (void)getByType:(PaymentType)type
        onSuccess:(void (^)(NSArray *))success
        onFailure:(void (^)(MLError *))failure {
    
    // Datos para url
    NSString *baseUrl = [[MLConfig shared] configuration:SERVER_BASE_URL];
    NSString *uri = PAYMENT_METHOD_URI;
    
    // Parametros
    NSString *publicKey = [[MLConfig shared] configuration:SERVER_PUBLIC_KEY];
    NSString *paymentTypeId = [PaymentMethod paymentMethodType2String:type];
    NSDictionary *parameters = [self queryParametersWithPublicKey:publicKey
                                                 andPaymentTypeId:paymentTypeId];
    
    // Ejecuto el servicio
    [[ApiService new] getFromBaseURL:baseUrl
                             withURI:uri
                       andParameters:parameters
                           onSuccess:^(NSArray *result) {
                               
        // Devuelvo el array de models
        success([PaymentMethod JSON2PaymentMethods:result]);
    } onFailure:^(MLError *error) {
        failure(error);
    }];
}

- (NSDictionary *)queryParametersWithPublicKey:(NSString *)publicKey
                              andPaymentTypeId:(NSString *)paymentTypeId {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    [parameters setValue:publicKey forKey:SERVER_PUBLIC_KEY_PARAM];
    [parameters setValue:paymentTypeId forKey:PAYMENT_METHOD_PAYMENT_TYPE_PARAM];
    return parameters;
}

@end
